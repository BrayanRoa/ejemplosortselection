/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.modelo.Persona;
import ufps.util.colecciones_seed.ExceptionUFPS;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author MADARME
 */
public class TestSort {
    
    public static void main(String[] args) throws ExceptionUFPS, Exception {
        /*ListaS<String> l=new ListaS();
        l.insertarAlFinal("GABRIEL");
        l.insertarAlFinal("JUAN");
        l.insertarAlFinal("ANA");
        l.insertarAlFinal("FARID");
        
        System.out.println("El menor de la lista es:"+l.getMenor());
        
        
        ListaS<Persona> personas=new ListaS();
        personas.insertarAlFinal(new Persona("Marco",1000));
       
        personas.insertarAlFinal(new Persona("Mariana",100));
        personas.insertarAlFinal(new Persona("Pepe",101));
         personas.insertarAlFinal(new Persona("Maria",10));
        
        System.out.println("El menor de la lista es:"+personas.getMenor());
        
        
        System.out.println("Lista Desordenada:"+personas.toString());
        personas.sortSelection();
        System.out.println("Lista Ordenada:"+personas.toString());*/
        
        TestSort pruebas = new TestSort();
        
        //long inicio =  System.nanoTime();
        
        pruebas.experimento2();
        //long ultimo = System.nanoTime();
        
        /*double dif=(double)(ultimo-inicio)*1.0e-9;
        System.out.println("El programa duró:"+dif+"s en ejecutarse");*/
        
        
        
        /*ListaS<Integer> l = new ListaS();
        
        
        l.insertarAlFinal(9);
        l.insertarAlFinal(7);
        l.insertarAlFinal(3);
        l.insertarAlFinal(985);
        l.insertarAlFinal(10);
        l.insertarAlFinal(1);
        l.insertarAlFinal(8);
        l.insertarAlFinal(1000);
        l.insertarAlFinal(11);
        l.insertarAlFinal(2);
        l.insertarAlFinal(4);
        l.insertarAlFinal(5);
        l.insertarAlFinal(6);
        
        System.out.println("LISTA DESORDENADA " + "\n" + l.toString());
        l.ordenarInsercion_Por_Nodos();
        System.out.println("LISTA ORDENADA " + "\n" + l.toString());
        */
        
    }
    
    public void experimento1(){
        long inicio =  System.nanoTime();
        
        ListaS<Integer> l = new ListaS();
        for(int i=0; i<20000; i++){
            l.insertarAlFinal(i);
        }
        System.out.println(l.toString());
        
        long ultimo = System.nanoTime();
        
        double dif=(double)(ultimo-inicio)*1.0e-9;
        System.out.println("El programa duró:"+dif+"s en ejecutarse");
    }
    
    public void experimento2(){
        long inicio =  System.nanoTime();
        
        ListaS<Integer> l = new ListaS();
        for(int i=0; i<200000; i++){
            l.insertarAlFinal(i);
        }
        System.out.println(l.toString());
        
        long ultimo = System.nanoTime();
        
        double dif=(double)(ultimo-inicio)*1.0e-9;
        System.out.println("El programa duró:"+dif+"s en ejecutarse");
    }
    
    
    public void experimento3(){
        
        long inicio =  System.nanoTime();
        
        ListaS<Integer> l = new ListaS();
        for(int i=0; i<5000000; i++){
            l.insertarAlFinal(i);
        }
        System.out.println(l.toString());
        
        long ultimo = System.nanoTime();
        
        double dif=(double)(ultimo-inicio)*1.0e-9;
        System.out.println("El programa duró:"+dif+"s en ejecutarse");
    }
    
    public void experimento4() throws Exception{
        
        long inicio =  System.nanoTime();
        
        ListaS l = new ListaS();
        
        for (int i = 0; i < 20000; i++) {
            l.insertarAlInicio("hola");
            l.insertarAlFinal("Nombre");
            
            if(i%2==0){
            l.insertarAlFinal("Santiago");
            l.insertarAlInicio(i);
            }
        }
        
        l.ordenarInsercion_Por_Nodos();
        System.out.println(this.toString());
        
        long ultimo = System.nanoTime();
        
        double dif=(double)(ultimo-inicio)*1.0e-9;
        System.out.println("El programa duró:"+dif+"s en ejecutarse");
    }
    
    
    public void experimento5() throws Exception{
        
        long inicio =  System.nanoTime();
        
        ListaS l = new ListaS();
        
        for (int i = 0; i < 200000; i++) {
            l.insertarAlInicio("hola");
            l.insertarAlFinal("Nombre");
            
            if(i%2==0){
            l.insertarAlFinal("Santiago");
            l.insertarAlInicio(i);
            }
        }
        
        l.ordenarInsercion_Por_Nodos();
        System.out.println(this.toString());
        
        long ultimo = System.nanoTime();
        
        double dif=(double)(ultimo-inicio)*1.0e-9;
        System.out.println("El programa duró:"+dif+"s en ejecutarse");
    }
    
    
    public void experimento6() throws Exception{
        
        long inicio =  System.nanoTime();
        
        ListaS l = new ListaS();
        
        for (int i = 0; i < 5000000; i++) {
            l.insertarAlInicio("hola");
            l.insertarAlFinal("Nombre");
            
            if(i%2==0){
            l.insertarAlFinal("Santiago");
            l.insertarAlInicio(i);
            }
        }
        
        l.ordenarInsercion_Por_Nodos();
        System.out.println(this.toString());
        
        long ultimo = System.nanoTime();
        
        double dif=(double)(ultimo-inicio)*1.0e-9;
        System.out.println("El programa duró:"+dif+"s en ejecutarse");
    }
   
    
}
