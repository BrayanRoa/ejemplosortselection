/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class ListaSTest {
    
    public ListaSTest() {
    }

    /**
     * Test of insertarAlInicio method, of class ListaS.
     */
    @Test
    public void testInsertarAlInicio() {
        System.out.println("Probando insertar al inicio");
        
        ListaS<Integer> l = new ListaS();
        l.insertarAlInicio(1);
        l.insertarAlInicio(2);
        
        
        boolean pasoPrueba = false;
        
        if(l.getTamanio()==2 && l.get(0)==2 && l.get(1)==1){
            pasoPrueba = true;
        }
        assertTrue(pasoPrueba);
    }

    /**
     * Test of insertarAlFinal method, of class ListaS.
     */
    @Test
    public void testInsertarAlFinal() {
        
        System.out.println("Probando insertar al final");
        
        ListaS<Integer> l = new ListaS();
        
        l.insertarAlFinal(9);
        l.insertarAlFinal(8);
        l.insertarAlFinal(7);
        l.insertarAlFinal(6);
        
        boolean pasoPrueba = false;
        
        if(l.getTamanio()==4 && (l.get(0)==9 && l.get(1)==8 && l.get(2)==7 && l.get(3)==6)){
            pasoPrueba = true;
        }
        assertTrue(pasoPrueba);
    }
    
    
    @Test
    public void test2InsertarAlFinal() {
        
        System.out.println("Probando insertar al final");
        
        ListaS<Integer> l = new ListaS();
        
        l.insertarAlFinal(1500);
        l.insertarAlFinal(145);
        l.insertarAlFinal(478);
        l.insertarAlFinal(100);
        l.insertarAlFinal(200);
        l.insertarAlFinal(750);
        l.insertarAlFinal(1200);
        l.insertarAlFinal(730);
        
        boolean pasoPrueba = false;
        
        if(l.getTamanio()==8 && (l.get(0)==1500 && l.get(1)==145 && l.get(2)==478 && l.get(3)==100
                && l.get(4)==200 && l.get(5)==750 && l.get(6)==1200 && l.get(7)==730)){
            pasoPrueba = true;
        }
        assertTrue(pasoPrueba);
    }

    /**
     * Test of insertarOrdenado method, of class ListaS.
     */
    @Test
    public void testInsertarOrdenado() {
    }

    /**
     * Test of eliminar method, of class ListaS.
     */
    @Test
    public void testEliminar() {
    }

    /**
     * Test of vaciar method, of class ListaS.
     */
    @Test
    public void testVaciar() {
    }

    /**
     * Test of get method, of class ListaS.
     */
    @Test
    public void testGet() {
    }

    /**
     * Test of set method, of class ListaS.
     */
    @Test
    public void testSet() {
        
        System.out.println("Probando Metodo set");
        
        ListaS<Integer> l = new ListaS();
        
        l.insertarAlFinal(1500);
        l.insertarAlFinal(145);
        l.insertarAlFinal(478);
        l.insertarAlFinal(100);
        
        l.set(0, 100);
        l.set(1, 150);
        l.set(2, 200);
        l.set(3, 250);
       
        boolean pasoPrueba = false;
        
        if(l.getTamanio()==4 && (l.get(0)==100) && l.get(1)==150 && l.get(2)==200 && l.get(3)==250){
            pasoPrueba = true;
        }
        
        assertTrue(pasoPrueba);
    }
    
    
    @Test
    public void test2Set() {
        
        System.out.println("Probando Metodo set");
        
        ListaS<Integer> l = new ListaS();
        
        l.insertarAlFinal(1500);
        l.insertarAlFinal(145);
        l.insertarAlFinal(478);
        l.insertarAlFinal(100);
        l.insertarAlFinal(15);
        l.insertarAlFinal(165);
        l.insertarAlFinal(48);
        l.insertarAlFinal(1800);
        l.insertarAlFinal(112);
        l.insertarAlFinal(43);
        l.insertarAlFinal(47);
        l.insertarAlFinal(10);
        
        l.set(0, 10);
        l.set(1, 20);
        l.set(2, 30);
        l.set(3, 40);
        l.set(4, 50);
        l.set(5, 60);
        l.set(6, 70);
        l.set(7, 80);
        l.set(8, 90);
        l.set(9, 100);
        l.set(10, 110);
        l.set(11, 120);
       
        boolean pasoPrueba = false;
        
        if(l.getTamanio()==12 && (l.get(0)==10) && l.get(1)==20 && l.get(2)==30 && l.get(3)==40 && l.get(4)==50
                              && l.get(5)==60 && l.get(6)==70 && l.get(7)==80 && l.get(8)==90 && l.get(9)==100
                              && l.get(10)==110 && l.get(11)==120){
            pasoPrueba = true;
        }
        
        assertTrue(pasoPrueba);
    }
    
    @Test
    public void testordenarInsercion_Por_Nodos(){
        
        System.out.println("ORDENAMIENTO POR NODOS");
        
        ListaS<Integer> l = new ListaS();
        boolean pasoPrueba=false;
        
        l.insertarAlFinal(5);
        l.insertarAlFinal(78);
        l.insertarAlFinal(47);
        l.insertarAlFinal(15);
        l.insertarAlFinal(20);
        l.insertarAlFinal(16);
        l.insertarAlFinal(2);
        l.insertarAlFinal(7);
        
        l.ordenarInsercion_Por_Nodos();
        
        if(l.getTamanio()==8 && (l.get(0)==2 && l.get(1)==5 && l.get(2)==7 && l.get(3)==15
                && l.get(4)==16 && l.get(5)==20 && l.get(6)==47 && l.get(7)==78)){
            pasoPrueba=true;
        }
        assertTrue(pasoPrueba);
    }
    
    
    public void test2ordenarInsercion_Por_Nodos(){
        
        System.out.println("ORDENAMIENTO POR NODOS 2");
        
        ListaS<Integer> l = new ListaS();
        boolean pasoPrueba=false;
        
        l.insertarAlFinal(5);
        l.insertarAlFinal(78);
        l.insertarAlFinal(47);
        l.insertarAlFinal(15);
        l.insertarAlFinal(20);
        l.insertarAlFinal(16);
        l.insertarAlFinal(2);
        l.insertarAlFinal(7);
        l.insertarAlFinal(31);
        l.insertarAlFinal(88);
        l.insertarAlFinal(97);
        l.insertarAlFinal(75);
        l.insertarAlFinal(22);
        l.insertarAlFinal(19);
        l.insertarAlFinal(4);
        l.insertarAlFinal(1);
        
        l.ordenarInsercion_Por_Nodos();
        
        if(l.getTamanio()==16 && (l.get(0)==1 && l.get(1)==2 && l.get(2)==4 && l.get(3)==5
                && l.get(4)==7 && l.get(5)==15 && l.get(6)==16 && l.get(7)==19 &&
                l.get(8)==20 && l.get(9)==22 && l.get(10)==31 && l.get(11)==47
                && l.get(12)==75 && l.get(13)==78 && l.get(14)==88 && l.get(15)==97)){
            pasoPrueba=true;
        }
        assertTrue(pasoPrueba);
    }

    /**
     * Test of getTamanio method, of class ListaS.
     */
    @Test
    public void testGetTamanio() {
    }

    /**
     * Test of esVacia method, of class ListaS.
     */
    @Test
    public void testEsVacia() {
    }

    /**
     * Test of esta method, of class ListaS.
     */
    @Test
    public void testEsta() {
    }

    /**
     * Test of iterator method, of class ListaS.
     */
    @Test
    public void testIterator() {
    }

    /**
     * Test of aVector method, of class ListaS.
     */
    @Test
    public void testAVector() {
    }

    /**
     * Test of toString method, of class ListaS.
     */
    @Test
    public void testToString() {
    }

    /**
     * Test of getIndice method, of class ListaS.
     */
    @Test
    public void testGetIndice() {
    }

    /**
     * Test of getMenor method, of class ListaS.
     */
    @Test
    public void testGetMenor() {
    }

    /**
     * Test of sortSelection method, of class ListaS.
     */
    @Test
    public void testSortSelection() {
    }

    /**
     * Test of hashCode method, of class ListaS.
     */
    @Test
    public void testHashCode() {
    }

    /**
     * Test of equals method, of class ListaS.
     */
    @Test
    public void testEquals() {
    }

    /**
     * Test of ordenarEjemplo method, of class ListaS.
     */
    

    /**
     * Test of buscar method, of class ListaS.
     */
    @Test
    public void testBuscar() {
    }

    /**
     * Test of existeInfo method, of class ListaS.
     */
    @Test
    public void testExisteInfo() {
    }
    
}
